var net = require("net");
var util = require('util');
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

//Game Vars
var race;
var pieces;
var lanes;
var ourCar;
var lastCarPositions;
var currentCarPositions;
var ourLastCarPosition;
var ourCurrentCarPosition;

//Physics Vars
var carRadius;
var carLength;
var carWidth;

//console.log(util.inspect(carPosition, { showHidden: true, depth: null }));
function isBend(piece){
  return piece.radius != undefined;
}

function angularAcceleration(velocity, torqueAngle, turnRadius){
  var torqueAngleR = torqueAngle * Math.PI / 180.0;
  //assume 90
  torqueAngleR = Math.PI / 2.0;
  var angularAccelerationR = (12 * carRadius * velocity * velocity * Math.sin(torqueAngleR))/(turnRadius * ((carLength * carLength) + (carWidth * carWidth) + (12 * carRadius * carRadius)));
  return angularAccelerationR * 180.0 / Math.PI;
}

function velocity(){
  if(ourLastCarPosition === undefined || ourCurrentCarPosition === undefined){
    return 0;
  }
  var lastPiece = ourLastCarPosition.piecePosition;
  var currentPiece = ourCurrentCarPosition.piecePosition;
  var velocity = 0;
  //last piece different from current piece
  if(lastPiece.pieceIndex != currentPiece.pieceIndex || lastPiece.lap != currentPiece.lap){
    //last piece distance WILL NOT WORK PERFECTLY FOR SWITCHING DURING CURVE
    if(isBend(pieces[lastPiece.pieceIndex])){
      var startLane = lastPiece.lane.startLaneIndex;
      var endLane = lastPiece.lane.endLaneIndex;
      var distanceFromCenter = (lanes[startLane].distanceFromCenter + lanes[endLane].distanceFromCenter)/2.0;
      var radius = pieces[lastPiece.pieceIndex].radius;
      if(pieces[lastPiece.pieceIndex].angle > 0){
        radius -= distanceFromCenter;
      }
      else{
        radius += distanceFromCenter;
      }
      velocity += ((2 * Math.PI * radius * Math.abs(pieces[lastPiece.pieceIndex].angle) / 360.0) - lastPiece.inPieceDistance);
    }
    else{
      velocity += race.track.pieces[lastPiece.pieceIndex].length - lastPiece.inPieceDistance;
    }
    velocity += currentPiece.inPieceDistance;
  }
  //last piece same as current piece
  else {
    velocity += currentPiece.inPieceDistance - lastPiece.inPieceDistance;
  }
  return velocity;
}

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    lastCarPositions = currentCarPositions;
    ourLastCarPosition = ourCurrentCarPosition;
    currentCarPositions = data.data;
    for(var i=0;i<currentCarPositions.length;i++){
      if(currentCarPositions[i].id.name === ourCar.name){
        ourCurrentCarPosition = currentCarPositions[i];
      }
    }
    send({
      msgType: "throttle",
      data: 0.6
    });
    var v = velocity();
    console.log("     velocity: " + v);
    if(ourCurrentCarPosition != undefined){
      if(isBend(pieces[ourCurrentCarPosition.piecePosition.pieceIndex])){
        var aa = angularAcceleration(v, 90, pieces[ourCurrentCarPosition.piecePosition.pieceIndex].radius);
        console.log("angular accel: " + aa);
      }
    }
    console.log("----------------------");
  }
  else {
    if (data.msgType === 'join') {
      console.log('Joined')
    }
    else if (data.msgType === 'gameStart') {
      console.log('Race started');
    }
    else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    }
    else if (data.msgType === 'yourCar') {
      console.log('Your car assigned');
      ourCar = data.data;
    }
    else if (data.msgType === 'gameInit') {
      console.log('Game initiated');
      race = data.data.race;
      console.log(util.inspect(race, { showHidden: true, depth: null }));
      pieces = race.track.pieces;
      lanes = race.track.lanes;
      for(var i=0;i<race.cars.length;i++){
        if(race.cars[i].id.name === ourCar.name){
          carLength = race.cars[i].dimensions.length;
          carWidth = race.cars[i].dimensions.width;
          carRadius = (carLength/2) - race.cars[i].dimensions.guideFlagPosition;
        }
      }
      console.log(ourCar);
    } 

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
